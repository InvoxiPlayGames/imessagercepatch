# iMessage RCE Patch

This tweak is not a patch, per se - it mitigates against an iMessage RCE exploit in a similar fashion to how Apple mitigated it in iOS 12.4.1 through 13.1.3.

It is VERY experimental and may cause a few slow downs or issues using iMessage (and iOS itself) - but anything's better than being l33t h4x0r'd.

For more details on the exploit this mitigates, check out Samuel Groß's writeup on the Google Project Zero blog: https://googleprojectzero.blogspot.com/2020/01/remote-iphone-exploitation-part-1.html

## How it works

This mitigates most of the exploit by disabling subclasses when `imagent` attempts to decode the iMessage data. For use in SpringBoard and MobileSMS, which also decode iMessage data, it prevents `NSSharedKeyDictionary` from being decoded (preventing all subclasses would cause random crashes in these processes)
