NSArray* blacklistedClasses;
BOOL shouldBlockClass(Class classToVerify) {
    for (Class class in blacklistedClasses) {
        if ([classToVerify isSubclassOfClass:class]) return YES;
    }
    return NO;
}

%group imagent
%hook NSKeyedUnarchiver
-(BOOL)_validateAllowedClassesContainsClass:(Class)arg1 forKey:(id)arg2 {
	if (![[self allowedClasses] containsObject:arg1] && shouldBlockClass(arg1)) return NO;
	return %orig;
}
-(BOOL)_validatePropertyListClass:(Class)arg1 forKey:(id)arg2 {
	if (![[self allowedClasses] containsObject:arg1] && shouldBlockClass(arg1)) return NO;
	return %orig;
}
%end
%end

%group SpringBoardAndMessages
%hook NSKeyedUnarchiver
-(BOOL)_validateAllowedClassesContainsClass:(Class)arg1 forKey:(id)arg2 {
	if (![[self allowedClasses] containsObject:arg1] && shouldBlockClass(arg1)) return NO;
	return %orig;
}
-(BOOL)_validatePropertyListClass:(Class)arg1 forKey:(id)arg2 {
	if (![[self allowedClasses] containsObject:arg1] && shouldBlockClass(arg1)) return NO;
	return %orig;
}
%end
%end

%ctor {
	blacklistedClasses = @[%c(NSSharedKeyDictionary), %c(NSKnownKeysDictionary1), %c(_NSDataFileBackedFuture), %c(_PFArray)];
	NSString *processName = [[NSProcessInfo processInfo] processName];
	if ([processName isEqualToString:@"imagent"]) {
		%init(imagent);
	} else {
		%init(SpringBoardAndMessages);
	}
}