INSTALL_TARGET_PROCESSES = SpringBoard imagent
ARCHS=arm64

include $(THEOS)/makefiles/common.mk

TWEAK_NAME = iMessageRCEPatch

iMessageRCEPatch_FILES = Tweak.x
iMessageRCEPatch_CFLAGS = -fobjc-arc
iMessageRCEPatch_FRAMEWORKS += CoreFoundation

include $(THEOS_MAKE_PATH)/tweak.mk
